require "azt_palindrome/version"

class String
	def palindrome?
		normalize == normalize.reverse
	end

	def letters
		self.chars.select { |c| c.match(/[a-z]/i) }.join
	end	

	private

	def normalize
		self.scan(/[a-z]/i).join.downcase
		#self.letters.downcase
	end
end
